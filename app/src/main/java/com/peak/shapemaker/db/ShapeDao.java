package com.peak.shapemaker.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.peak.shapemaker.vo.Shape;

import java.util.List;


/**
 * Interface for database access for User related operations.
 */
@Dao
public interface ShapeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Shape shape);

    @Delete
    void delete(Shape shape);

    @Query("SELECT * FROM shape")
    LiveData<List<Shape>> get();

    @Query("SELECT * FROM shape where type = 0")
    LiveData<List<Shape>> getSquares();

    @Query("SELECT * FROM shape where type = 1")
    LiveData<List<Shape>> getCircles();

    @Query("SELECT * FROM shape where type = 2")
    LiveData<List<Shape>> getTriangles();

    @Query("DELETE FROM shape where type = 0")
    void deleteSquares();

    @Query("DELETE FROM shape where type = 1")
    void deleteCircles();

    @Query("DELETE FROM shape where type = 2")
    void deleteTriangles();
}
