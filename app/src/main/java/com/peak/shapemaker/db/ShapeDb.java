package com.peak.shapemaker.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.peak.shapemaker.vo.Shape;

/**
 * Main database description.
 */
@Database(entities = {Shape.class}, version = 1)
public abstract class ShapeDb extends RoomDatabase {

    abstract public ShapeDao shapeDao();
}
