package com.peak.shapemaker.db;

import android.arch.persistence.room.TypeConverter;

import com.peak.shapemaker.vo.Shape;

/**
 * Created by Mamata on 6/6/2018.
 */

public class ShapeTypeConverter {
    @TypeConverter
    public static Shape.Type toType(int type) {
        return Shape.Type.values()[type];
    }

    @TypeConverter
    public static int toOrdinal(Shape.Type type) {
        return type.getCode();
    }
}
