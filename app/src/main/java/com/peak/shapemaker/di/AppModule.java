package com.peak.shapemaker.di;

import android.app.Application;
import android.arch.persistence.room.Room;


import com.peak.shapemaker.db.ShapeDao;
import com.peak.shapemaker.db.ShapeDb;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ViewModelModule.class)
class AppModule {

    @Singleton
    @Provides
    ShapeDb provideDb(Application app) {
        return Room.databaseBuilder(app, ShapeDb.class, "shapemaker.db").build();
    }

    @Provides
    ShapeDao getShapeDao(ShapeDb shapeDb) {
        return shapeDb.shapeDao();
    }
}
