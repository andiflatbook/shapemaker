package com.peak.shapemaker.di;


import com.peak.shapemaker.ui.canvas.CanvasFragment;
import com.peak.shapemaker.ui.states.StateFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract CanvasFragment contributeCanvasFragment();

    @ContributesAndroidInjector
    abstract StateFragment contributeStatsFragement();
}
