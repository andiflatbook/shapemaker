package com.peak.shapemaker.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.peak.shapemaker.ui.common.ShapeMakerViewModelFactory;
import com.peak.shapemaker.ui.common.ShapeViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ShapeViewModel.class)
    abstract ViewModel bindCanvasViewModel(ShapeViewModel shapeViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ShapeMakerViewModelFactory factory);
}
