package com.peak.shapemaker.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.peak.shapemaker.AppExecutors;
import com.peak.shapemaker.db.ShapeDao;
import com.peak.shapemaker.repository.task.AddShapeTask;
import com.peak.shapemaker.repository.task.DeleteShapeTask;
import com.peak.shapemaker.repository.task.DeleteTypedShapesTask;
import com.peak.shapemaker.vo.Resource;
import com.peak.shapemaker.vo.Shape;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Repository that handles User objects.
 */
@Singleton
public class ShapeRepository {
    private final ShapeDao shapeDao;
    private final AppExecutors appExecutors;

    @Inject
    ShapeRepository(AppExecutors appExecutors, ShapeDao shapeDao) {
        this.shapeDao = shapeDao;
        this.appExecutors = appExecutors;
    }

    public LiveData<Resource<List<Shape>>> loadStates() {
        return new NetworkBoundResource<List<Shape>>() {
            @NonNull
            @Override
            protected LiveData<List<Shape>> loadFromDb() {
                return shapeDao.get();
            }

        }.asLiveData();
    }

    public LiveData<Resource<List<Shape>>> loadSquares() {
        return new NetworkBoundResource<List<Shape>>() {
            @NonNull
            @Override
            protected LiveData<List<Shape>> loadFromDb() {
                return shapeDao.getSquares();
            }

        }.asLiveData();
    }

    public LiveData<Resource<List<Shape>>> loadCircles() {
        return new NetworkBoundResource<List<Shape>>() {
            @NonNull
            @Override
            protected LiveData<List<Shape>> loadFromDb() {
                return shapeDao.getCircles();
            }

        }.asLiveData();
    }

    public LiveData<Resource<List<Shape>>> loadTriangles() {
        return new NetworkBoundResource<List<Shape>>() {
            @NonNull
            @Override
            protected LiveData<List<Shape>> loadFromDb() {
                return shapeDao.getTriangles();
            }

        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> addShape(Shape shape) {
        AddShapeTask addShapeTask = new AddShapeTask(shape, shapeDao);
        appExecutors.networkIO().execute(addShapeTask);
        return addShapeTask.getLiveData();
    }

    public LiveData<Resource<Boolean>> deleteShape(Shape shape) {
        DeleteShapeTask deleteShapeTask = new DeleteShapeTask(shape, shapeDao);
        appExecutors.networkIO().execute(deleteShapeTask);
        return deleteShapeTask.getLiveData();
    }

    public LiveData<Resource<Boolean>> deleteTypedShapes(Shape.Type type) {
        DeleteTypedShapesTask deleteTypedShapesTask = new DeleteTypedShapesTask(type, shapeDao);
        appExecutors.networkIO().execute(deleteTypedShapesTask);
        return deleteTypedShapesTask.getLiveData();
    }
}
