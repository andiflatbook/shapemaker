package com.peak.shapemaker.repository.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.peak.shapemaker.db.ShapeDao;
import com.peak.shapemaker.vo.Resource;
import com.peak.shapemaker.vo.Shape;

/**
 * A task that reads the search result in the database and fetches the next page, if it has one.
 */
public class DeleteShapeTask implements Runnable {
    private final MutableLiveData<Resource<Boolean>> liveData = new MutableLiveData<>();
    private final Shape shape;
    private final ShapeDao shapeDao;

    public DeleteShapeTask(Shape shape, ShapeDao shapeDao) {
        this.shape = shape;
        this.shapeDao = shapeDao;
    }

    @Override
    public void run() {
        shapeDao.delete(shape);
        liveData.postValue(Resource.success(true));
    }

    public LiveData<Resource<Boolean>> getLiveData() {
        return liveData;
    }
}
