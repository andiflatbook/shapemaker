package com.peak.shapemaker.repository.task;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.peak.shapemaker.db.ShapeDao;
import com.peak.shapemaker.vo.Resource;
import com.peak.shapemaker.vo.Shape;

/**
 * A task that reads the search result in the database and fetches the next page, if it has one.
 */
public class DeleteTypedShapesTask implements Runnable {
    private final MutableLiveData<Resource<Boolean>> liveData = new MutableLiveData<>();
    private final ShapeDao shapeDao;
    private Shape.Type type;

    public DeleteTypedShapesTask(Shape.Type type, ShapeDao shapeDao) {
        this.type = type;
        this.shapeDao = shapeDao;
    }

    @Override
    public void run() {

        switch (type) {
            case TYPE_SQUARE:
                shapeDao.deleteSquares();
                break;
            case TYPE_CIRCLE:
                shapeDao.deleteCircles();
                break;
            case TYPE_TRIANGLE:
                shapeDao.deleteTriangles();
                break;
        }

        liveData.postValue(Resource.success(true));
    }

    public LiveData<Resource<Boolean>> getLiveData() {
        return liveData;
    }
}
