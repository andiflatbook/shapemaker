package com.peak.shapemaker.ui.canvas;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.peak.shapemaker.R;
import com.peak.shapemaker.constants.Constants;
import com.peak.shapemaker.di.Injectable;
import com.peak.shapemaker.ui.common.ShapeViewModel;
import com.peak.shapemaker.ui.custom.CustomShapeView;
import com.peak.shapemaker.vo.Shape;

import java.util.Random;

import javax.inject.Inject;


/**
 * Created by Mamata on 6/4/2018.
 */

public class CanvasFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ShapeViewModel shapeViewModel;
    private View rootView;
    private FrameLayout canvas;
    private CustomShapeView customShapeView;

    private Random random = new Random();

    public static CanvasFragment create() {
        CanvasFragment canvasFragment = new CanvasFragment();
        Bundle bundle = new Bundle();
        //Put arguments here if available
        canvasFragment.setArguments(bundle);
        return canvasFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_canvas, null, false);
        canvas = rootView.findViewById(R.id.canvas);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView.findViewById(R.id.btn_circle).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.btn_square).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.btn_triangle).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.btn_undo).setOnClickListener(onClickListener);

        customShapeView = new CustomShapeView(getActivity());
        canvas.addView(customShapeView);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shapeViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShapeViewModel.class);

        shapeViewModel.getAllShapes().observe(this, listResource -> {
            if (listResource != null) {
                switch (listResource.status) {
                    case ERROR:
                        // Handle Error scenario here
                        break;
                    case LOADING:
                        // Show loading dialog here
                        break;
                    case SUCCESS:
                        customShapeView.updateShapes(listResource.data);
                        break;
                }
            }
        });
    }

    private View.OnClickListener onClickListener = view -> {

        switch (view.getId()) {
            case R.id.btn_circle:
                addShape(Shape.Type.TYPE_CIRCLE);
                break;
            case R.id.btn_square:
                addShape(Shape.Type.TYPE_SQUARE);
                break;
            case R.id.btn_triangle:
                addShape(Shape.Type.TYPE_TRIANGLE);
                break;
            case R.id.btn_undo:
                if (customShapeView.getLastShape() != null)
                    shapeViewModel.deleteShape(customShapeView.getLastShape());
                break;
        }
    };

    private void addShape(Shape.Type type) {
        Shape shape = new Shape();
        shape.setRadius(Constants.RADIUS);
        shape.setX(random.nextInt(canvas.getWidth() - Constants.RADIUS));
        shape.setY(random.nextInt(canvas.getHeight() - Constants.RADIUS));
        shape.setType(type);
        shapeViewModel.addShape(shape);
    }
}
