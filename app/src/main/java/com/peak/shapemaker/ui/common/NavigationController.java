package com.peak.shapemaker.ui.common;

import android.support.v4.app.FragmentManager;

import com.peak.shapemaker.R;
import com.peak.shapemaker.ui.MainActivity;
import com.peak.shapemaker.ui.canvas.CanvasFragment;
import com.peak.shapemaker.ui.states.StateFragment;

import javax.inject.Inject;

/**
 * A utility class that handles navigation in {@link MainActivity}.
 */
public class NavigationController {
    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToCanvas() {
        CanvasFragment canvasFragment = CanvasFragment.create();
        fragmentManager.beginTransaction()
                .replace(containerId, canvasFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToStates() {
        StateFragment stateFragment = StateFragment.create();
        fragmentManager.beginTransaction()
                .replace(containerId, stateFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
