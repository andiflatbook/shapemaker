package com.peak.shapemaker.ui.common;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;

import com.peak.shapemaker.repository.ShapeRepository;
import com.peak.shapemaker.vo.Resource;
import com.peak.shapemaker.vo.Shape;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Mamata on 5/19/2018.
 */

public class ShapeViewModel extends ViewModel {
    @VisibleForTesting
    private LiveData<Resource<List<Shape>>> mAllShapes;
    private LiveData<Resource<List<Shape>>> squares;
    private LiveData<Resource<List<Shape>>> circles;
    private LiveData<Resource<List<Shape>>> triangles;
    private ShapeRepository shapeRepository;

    @SuppressWarnings("unchecked")
    @Inject
    public ShapeViewModel(ShapeRepository shapeRepository) {
        this.shapeRepository = shapeRepository;
        mAllShapes = this.shapeRepository.loadStates();
        squares = this.shapeRepository.loadSquares();
        circles = this.shapeRepository.loadCircles();
        triangles = this.shapeRepository.loadTriangles();
    }

    public LiveData<Resource<List<Shape>>> getAllShapes() {
        return mAllShapes;
    }

    public void addShape(Shape shape) {
        this.shapeRepository.addShape(shape);
    }

    public void deleteShape(Shape shape) {
        this.shapeRepository.deleteShape(shape);
    }

    public LiveData<Resource<List<Shape>>> getSquares() {
        return squares;
    }

    public LiveData<Resource<List<Shape>>> getCircles() {
        return circles;
    }

    public LiveData<Resource<List<Shape>>> getTriangles() {
        return triangles;
    }

    public void deleteSquares() {
        this.shapeRepository.deleteTypedShapes(Shape.Type.TYPE_SQUARE);
    }

    public void deleteCircles() {
        this.shapeRepository.deleteTypedShapes(Shape.Type.TYPE_CIRCLE);
    }

    public void deleteTriangles() {
        this.shapeRepository.deleteTypedShapes(Shape.Type.TYPE_TRIANGLE);
    }
}
