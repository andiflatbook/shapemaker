package com.peak.shapemaker.ui.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import com.peak.shapemaker.vo.Shape;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mamata on 6/6/2018.
 */

public class CustomShapeView extends View {

    private Paint paint;
    private Path path;

    private ArrayList<Shape> shapes = new ArrayList<>();

    public CustomShapeView(Context context) {
        super(context);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        path = new Path();

        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(100);
        paint.setStyle(Paint.Style.FILL);
    }

    public void updateShapes(List<Shape> shapes) {
        this.shapes.clear();
        this.shapes.addAll(shapes);
        invalidate();
    }

    public Shape getLastShape() {
        if (shapes.size() > 0) {
            return shapes.get(shapes.size() - 1);
        }
        return null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (Shape shape : shapes) {
            int randX = shape.getX();
            int randY = shape.getY();
            int radius = shape.getRadius();
            paint.setColor(shape.getType().getColor());

            switch (shape.getType()) {
                case TYPE_CIRCLE:
                    canvas.drawCircle(randX, randY, radius, paint);
                    break;
                case TYPE_SQUARE:
                    canvas.drawRect(randX - radius, randY - radius,
                            randX + radius, randY + radius, paint);
                    break;
                case TYPE_TRIANGLE:
                    path.reset();
                    path.moveTo(randX, randY - radius); // Top
                    path.lineTo(randX - radius, randY + radius); // Bottom left
                    path.lineTo(randX + radius, randY + radius); // Bottom right
                    path.lineTo(randX, randY - radius); // Back to Top
                    path.close();
                    canvas.drawPath(path, paint);
                    break;
            }
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            updateShape((int) event.getX(), (int) event.getY());
            return performClick();
        }
        return true;
    }

    private void updateShape(int x, int y) {
        Rect rect = new Rect(x - 50, y - 50,
                x + 50, y + 50);

        for (Shape shape : shapes) {
            if (rect.contains(shape.getX(), shape.getY())) {
                shape.setType(getUpdatedShape(shape.getType()));
            }
        }

        invalidate();
    }

    private Shape.Type getUpdatedShape(Shape.Type currentType) {
        switch (currentType) {
            case TYPE_CIRCLE:
                return Shape.Type.TYPE_SQUARE;
            case TYPE_SQUARE:
                return Shape.Type.TYPE_TRIANGLE;
            case TYPE_TRIANGLE:
                return Shape.Type.TYPE_CIRCLE;
            default:
                return Shape.Type.TYPE_CIRCLE;
        }
    }
}
