package com.peak.shapemaker.ui.states;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.peak.shapemaker.R;
import com.peak.shapemaker.di.Injectable;
import com.peak.shapemaker.ui.common.ShapeViewModel;

import javax.inject.Inject;


/**
 * Created by Mamata on 6/4/2018.
 */

public class StateFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ShapeViewModel shapeViewModel;
    private View rootView;


    public static StateFragment create() {
        StateFragment stateFragment = new StateFragment();
        Bundle bundle = new Bundle();
        stateFragment.setArguments(bundle);
        return stateFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_states, null, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnDeleteCircle).setOnClickListener(onClickListener);
        view.findViewById(R.id.btnDeleteSquare).setOnClickListener(onClickListener);
        view.findViewById(R.id.btnDeleteTriangle).setOnClickListener(onClickListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shapeViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShapeViewModel.class);

        shapeViewModel.getCircles().observe(this, listResource -> {
            if (listResource != null) {
                switch (listResource.status) {
                    case SUCCESS:
                        ((TextView) rootView.findViewById(R.id.countCircle)).setText(listResource.data.size() + "");
                        break;
                }
            }
        });

        shapeViewModel.getSquares().observe(this, listResource -> {
            if (listResource != null) {
                switch (listResource.status) {
                    case SUCCESS:
                        ((TextView) rootView.findViewById(R.id.countSquare)).setText(listResource.data.size() + "");
                        break;
                }
            }
        });

        shapeViewModel.getTriangles().observe(this, listResource -> {
            if (listResource != null) {
                switch (listResource.status) {
                    case SUCCESS:
                        ((TextView) rootView.findViewById(R.id.countTriangle)).setText(listResource.data.size() + "");
                        break;
                }
            }
        });
    }

    private View.OnClickListener onClickListener = view -> {
        switch (view.getId()) {
            case R.id.btnDeleteCircle:
                shapeViewModel.deleteCircles();
                break;
            case R.id.btnDeleteSquare:
                shapeViewModel.deleteSquares();
                break;
            case R.id.btnDeleteTriangle:
                shapeViewModel.deleteTriangles();
                break;
        }
    };
}
