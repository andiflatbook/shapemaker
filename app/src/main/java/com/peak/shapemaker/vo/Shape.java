package com.peak.shapemaker.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.graphics.Color;

import com.peak.shapemaker.db.ShapeTypeConverter;

/**
 * Created by Mamata on 6/4/2018.
 */

@Entity
public class Shape {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int x;
    private int y;

    @TypeConverters(ShapeTypeConverter.class)
    private Type type;

    private int radius;

    public enum Type {
        TYPE_SQUARE(0),
        TYPE_CIRCLE(1),
        TYPE_TRIANGLE(2);

        private int code;
        private int color;

        Type(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public int getColor() {
            switch (code) {
                case 0: // SQUARE #3F51B5 INDIGO
                    return Color.rgb(63, 81, 181);
                case 1: // CIRCLE #E91E63 PINK
                    return Color.rgb(233, 30, 99);
                case 2: // TRIANGLE #009688 TEAL
                    return Color.rgb(0, 150, 136);

            }
            return 0;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
